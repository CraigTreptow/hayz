module ClockFace ( plotClockFace ) where

import Canvas
import Color
import Data.Matrix
import Matrix
import Tuple
import Debug.Trace as DT

plotClockFace = do
  DT.traceIO ("START clock")
  let black = newColor 0 0 0
  let white = newColor 1 1 1
  let red   = newColor 1 0 0

  let size     = 100.0
  let halfSize = size / 2.0
  let c        = newCanvas (round size) (round size) black
  let origin   = newPoint halfSize halfSize halfSize
  let radius   = (3 / 8) * (fromIntegral (width c))
  let originColor = red
  let numberColor = white

  let canvasWithOrigin = writePixel c (round(x origin)) (round(y origin)) originColor
  let finalC           = plotHours canvasWithOrigin origin numberColor radius 1

  DT.trace ("FINISH") writeFile "./clock.ppm" (canvasToPPM finalC)

plotHours canvas _ _ _ 13 = canvas
plotHours canvas origin color radius hour = plotHours updatedC origin color radius (hour + 1)
  where p = computePoint origin radius hour
        updatedC = writePixel canvas (round(x p)) (round(y p)) color

computePoint origin radius hour = p
  where twelve = (newPoint 0 0 1)
        rotatedP = (rotationFor hour) `multipliedByTuple` twelve
        radiusAppliedP = applyRadius rotatedP radius
        p = moveToCenter radiusAppliedP origin

moveToCenter p origin = newPoint newX newY newZ
  where newX = (x p) + (x origin)
        newY = (z p) + (y origin)
        newZ = (z p) + (z origin)

rotationFor :: Double -> (Data.Matrix.Matrix Double)
rotationFor hour = rotationY (hour * (pi/6))

applyRadius :: Tuple -> Double -> Tuple
applyRadius p r = newPoint ((x p) * r) (y p) ((z p) * r)

module Projectile ( plotProjectile ) where

import Canvas
import Color
import Data.Matrix
import Debug.Trace as DT
import Matrix
import Tuple as T

plotProjectile = do
  DT.traceIO ("START tick")
  let black = newColor 0 0 0
  let c = newCanvas 900 550 black

  -- gravity -0.1 unit/tick, and wind is -0.01 unit/tick
  let initialGravity = newVector 0 (-0.1) 0
  let initialWind = newVector (-0.01) 0 0

  let env = (initialGravity, initialWind)

  -- projectile starts one unit above the origin
  -- velocitiy is normalized to one unit/tick
  let initialPoint = newPoint 0 1 0
  let initialVelocity = (normalize (newVector 1 1.8 0)) `T.multipliedBy` 11.25

  let initialProjectile = (initialPoint, initialVelocity)

  DT.trace ("Env: " ++ (show env) ++ "\n" ++ "InitProj: " ++ (show initialProjectile) ++ "\n") tick c env initialProjectile

tick :: Canvas -> (Tuple, Tuple) -> (Tuple, Tuple) -> IO ()
tick c e p
  | currentY >= 0 = DT.trace ("currentY " ++ (show currentY)) tick newCanvas e newProjectile
  | otherwise = DT.trace ("FINISH") writeFile "./clock.ppm" (canvasToPPM c)

  where red           = newColor 1 0 0
        point         = fst p
        velocity      = snd p
        gravity       = fst e
        wind          = snd e
        currentY      = y point
        newPoint      = point `T.addedTo` velocity
        newVelocity   = velocity `T.addedTo` (gravity `T.addedTo` wind)
        newProjectile = (newPoint, newVelocity)
        newCanvas     = writePixel c canvasX canvasY red
        canvasX       = round (x newPoint)
        canvasY       = (height c) - (round (y newPoint))


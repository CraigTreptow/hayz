module Sphere ( plotSphere ) where

import Canvas
import Color
import Debug.Trace as DT
import Tuple as T
import RayShape as R
import System.Random
import Material
import Light

plotSphere = do
  DT.traceIO ("START sphere")

  let black         = newColor 0 0 0
  let canvasPixels  = 300.0
  let initialCanvas = newCanvas (round canvasPixels) (round canvasPixels) black
  let finalCanvas   = castRaysOntoSphere initialCanvas

  DT.trace ("FINISH") writeFile "./sphere.ppm" (canvasToPPM finalCanvas)

castRaysOntoSphere :: Canvas -> Canvas
castRaysOntoSphere canvas = processColumn canvas 0 0

processColumn :: Canvas -> Double -> Double -> Canvas
processColumn canvas x y
  | y > maxY = canvas
  | otherwise = processRow canvas worldY x y
  where canvasPixels = fromIntegral $ width canvas
        maxY         = canvasPixels
        wallSize     = 7.0 :: Double
        halfWallSize = wallSize / 2
        pixelSize    = wallSize / canvasPixels
        worldY       = halfWallSize - (pixelSize * y)

processRow :: Canvas -> Double -> Double -> Double -> Canvas
processRow canvas worldY x y
  | x > maxX  = processColumn updatedCanvas 0 (y + 1)
  | otherwise = processRow updatedCanvas worldY newX y
  where canvasPixels = fromIntegral $ width canvas
        newX         = x + 1
        maxX         = canvasPixels - 1
        rayOrigin    = newPoint 0 0 (-5)
        defaultShape = newSphere 1
        pinkish      = newColor 1 0.2 1
        mat          = defaultMaterial { color = pinkish }
        shape        = setMaterial defaultShape mat
        wallSize     = 7.0 :: Double
        wallZ        = 10
        pixelSize    = wallSize / canvasPixels
        halfWallSize = wallSize / 2
        worldX       = (-halfWallSize) + (pixelSize * x)
        position     = newPoint worldX worldY wallZ
        r            = ray rayOrigin (normalize (rayOrigin `T.subtractedFrom` position))
        xs           = intersect shape r
        h            = hit xs 
        updatedCanvas = case h of
                          Just h  -> writePixel canvas (round x) (round y) (calcNewLightColor h r)
                          Nothing -> canvas

calcNewLightColor hit ray = newLightColor
  where white         = newColor 1 1 1
        lightPosition = newPoint (-10) 10 (-10)
        lightColor    = white
        light         = newPointLight lightPosition lightColor
        point        = R.position ray (tValue hit)
        theNormal     = normalAt (object hit) point
        eye          = neg (direction ray)
        obj           = object hit
        newLightColor = lighting (material obj) light point eye theNormal

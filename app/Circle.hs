module Circle ( plotCircle ) where

import Canvas
import Color
import Debug.Trace as DT
import Tuple as T
import RayShape
import System.Random

plotCircle = do
  DT.traceIO ("START circle")
  seed              <- randomRIO (1, maxBound :: Int)
  let shape         = newSphere seed
  let red           = newColor 1 0 0
  let black         = newColor 0 0 0
  let canvasPixels  = 200.0
  let initialCanvas = newCanvas (round canvasPixels) (round canvasPixels) black

  -- Start the Ray at z of -5
  let rayOrigin     = newPoint 0 0 (-5)
  -- put the wall z of 10
  let wallZ         = 10
  let wallSize      = 7.0
  let halfWallSize  = wallSize / 2
  let pixelSize     = wallSize / canvasPixels

  let finalCanvas = castRaysOntoSphere initialCanvas

  DT.trace ("FINISH") writeFile "./circle.ppm" (canvasToPPM finalCanvas)

castRaysOntoSphere :: Canvas -> Canvas
castRaysOntoSphere canvas = processColumn canvas 0 0

processColumn :: Canvas -> Double -> Double -> Canvas
processColumn canvas x y
  | y > maxY = canvas
  | otherwise = processRow canvas worldY x y
  where canvasPixels = fromIntegral $ width canvas
        maxY         = canvasPixels
        wallSize     = 7.0 :: Double
        halfWallSize = wallSize / 2
        pixelSize    = wallSize / canvasPixels
        worldY       = halfWallSize - (pixelSize * y)

processRow :: Canvas -> Double -> Double -> Double -> Canvas
processRow canvas worldY x y
  | x > maxX  = processColumn updatedCanvas 0 (y + 1)
  | otherwise = processRow updatedCanvas worldY newX y
  where canvasPixels = fromIntegral $ width canvas
        newX      = x + 1
        maxX      = canvasPixels - 1
        rayOrigin = newPoint 0 0 (-5)
        red       = newColor 1 0 0
        wallSize     = 7.0 :: Double
        wallZ        = 10
        pixelSize    = wallSize / canvasPixels
        halfWallSize = wallSize / 2
        worldX       = (-halfWallSize) + (pixelSize * x)
        position     = newPoint worldX worldY wallZ
        r            = ray rayOrigin (normalize (rayOrigin `T.subtractedFrom` position))
        shape        = newSphere 1
        xs = intersect shape r
        i = hit xs 
        updatedCanvas = case i of
                          Just i  -> writePixel canvas (round x) (round y) red
                          Nothing -> canvas

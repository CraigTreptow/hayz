# hayz

[![pipeline status](https://gitlab.com/CraigTreptow/hayz/badges/master/pipeline.svg)](https://gitlab.com/CraigTreptow/hayz/-/commits/master)

A ray tracer built with Haskell using the book ["The Ray Tracer Challenge"](http://raytracerchallenge.com/)

## How To Build

`stack build`

`stack test`

`stack exec hayz-exe`

## How To Run

`clear; stack run && open ./hayz.ppm`

That assumes you are on OSX, but might work other places.

## To Do

- [ ] use recursion in matrix transformations
- [ ] writePixel should not allow writes to out of bounds indices
- [ ] run code through a formatter
- [ ] qualify imports to only what I use
- [ ] put back X, Y, Z, W type aliases in Tuple

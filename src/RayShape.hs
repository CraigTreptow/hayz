module RayShape ( Ray (..)
                , Sphere (..)
                , Intersection (..)
                , normalAt
                , newSphere
                , intersect
                , setTransformation
                , setMaterial
                , hit
                , ray
                , position
                , transform
                ) where

import Color -- TODO: delete?
import Material
import Matrix
import Data.Matrix
import Tuple as T
import Data.List (sort)
import Data.Ord (comparing)
import System.Random


data Ray = MkRay { origin :: Tuple
                 , direction :: Tuple
                 } deriving (Show)

data Sphere = MkSphere { identifier :: Int
                       , transformation :: (Matrix Double)
                       , material :: Material
                       } deriving (Show, Eq)

data Intersection = MkIntersection { tValue :: Double
                                   , object  :: Sphere
                                   } deriving (Show, Eq)

instance Ord Intersection where
  compare a b = compare (tValue a) (tValue b)

setMaterial :: Sphere -> Material -> Sphere
setMaterial s mat = s { material = mat }

normalAt :: Sphere -> Tuple -> Tuple
normalAt s worldPoint = T.normalize worldNormal
  where origin = T.newPoint 0 0 0
        transform = transformation s
        invT = invert transform
        objectPoint = invT `multipliedByTuple` worldPoint
        objectNormal = (newPoint 0 0 0) `T.subtractedFrom` objectPoint
        wn =  (transpose invT) `multipliedByTuple` objectNormal
        -- TODO get this hack to work as it should
        -- transform = subMatrix 3 3 (transformation s)
        worldNormal = newVector (x wn) (y wn) (z wn)

setTransformation :: Sphere -> (Matrix Double) -> Sphere
setTransformation s t = s { transformation = t }

transform :: Ray -> (Matrix Double) -> Ray
transform r m = ray newOrigin newDirection
  where newOrigin =  (m `Matrix.multipliedByTuple` (origin r))
        newDirection = (m `Matrix.multipliedByTuple` (direction r))

position :: Ray -> Double -> Tuple
position r t = orig `T.addedTo` (direct `T.multipliedBy` t)
  where orig = origin r
        direct = direction r

ray :: Tuple -> Tuple -> Ray
ray origin vector = MkRay { origin = origin, direction = vector }

hit :: [Intersection] -> Maybe Intersection
hit intersections =
  case (is) of
    [] -> Nothing
    otherwise -> Just (head is)
  where
    is = filter (\x -> (tValue x) > 0) sortedIntersections
    sortedIntersections = sort intersections

intersect :: Sphere -> Ray -> [Intersection]
intersect sphere ray =
  if (discriminant < 0)
     then []
     else [MkIntersection { tValue = (head tValues), object = sphere }
          ,MkIntersection { tValue = (head $ tail tValues), object = sphere }]
  where a = dot (direction ray2) (direction ray2)
        b = 2 * (dot (direction ray2) sphereToRay)
        c = (dot sphereToRay sphereToRay) - 1
        t1 = ((-b) - (sqrt discriminant)) / (2 * a)
        t2 = ((-b) + (sqrt discriminant)) / (2 * a)
        tValues = sort [t1, t2]
        -- the vector from the sphere's center, to the ray origin
        -- remember: the sphere is centered at the world origin
        sphereToRay = (newPoint 0 0 0) `T.subtractedFrom` (origin ray2)
        discriminant = (b * b) - (4 * a * c)
        ray2 = transform ray $ invert (transformation sphere)

newSphere :: Int -> Sphere
newSphere id = MkSphere { identifier = id, transformation = identity 4, material = defaultMaterial }


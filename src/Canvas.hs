module Canvas ( Canvas (..)
              , newCanvas
              , canvasToPPM
              , height
              , pixelAt
              , width
              , writePixel
             ) where

import Color
import Data.Array
import Data.List as L
import Data.List.Utils (replace)
import Data.List.Split as LS
import Debug.Trace

type Canvas = Array (Int, Int) Color

newCanvas :: Int -> Int -> Color -> Canvas
newCanvas columns rows color =
  listArray ((0,0),(columns - 1, rows - 1)) (replicate size color)
    where x = columns
          y = rows
          size = x * y

canvasToPPM :: Canvas -> String
canvasToPPM c =
  (canvasHeader c) ++ "\n" ++
    (canvasBody c) ++ canvasFooter

height :: Canvas -> Int
height c = rows + 1
  where (lowerBound,upperBound) = bounds c
        cols = fst upperBound
        rows = snd upperBound

pixelAt :: Canvas -> Int -> Int -> Color
pixelAt c x y = c ! (x,y)

width :: Canvas -> Int
width c = cols + 1
  where (lowerBound,upperBound) = bounds c
        cols = fst upperBound
        rows = snd upperBound

writePixel :: Canvas -> Int -> Int -> Color -> Canvas
writePixel canvas column row color
  | column > (width canvas)  = canvas
  | row    > (height canvas) = canvas
  | otherwise                = canvas // [((column,row), color)]

-- Private --

canvasHeader :: Canvas -> String
canvasHeader c =
  "P3\n" ++ (show (width c)) ++ " " ++ (show (height c)) ++ "\n255"

canvasFooter :: String
canvasFooter = "\n"

clamp :: Double -> Int
clamp color_value
  | adjusted_value < 0     = min
  | adjusted_value > 255.0 = max
  | otherwise              = round adjusted_value
    where adjusted_value = color_value * (255.0 + 0.99)
          min            = 0 :: Int
          max            = 255 :: Int

canvasBody :: Canvas -> String
canvasBody c =
  replace "\n " "\n" $ replace " \n" "\n" $ unwords iChunks
    where maxY = (height c) - 1
          maxX = (width c) - 1
          chunks = LS.chunksOf (width c) [(showPixel $ (pixelAt c x y)) | x <- [0..maxX], y <- [0..maxY]]
          iChunks = L.intercalate ["\n"] chunks

showPixel p =
  (show r) ++ " " ++ (show g) ++ " " ++ (show b)
    where r = clamp (red p)
          g = clamp (green p)
          b = clamp (blue p)


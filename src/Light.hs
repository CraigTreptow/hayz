module Light ( PointLight (..)
             , newPointLight
             ) where

import Color
import Tuple

data PointLight = MkPointLight { position :: Tuple
                               , intensity :: Color
                               } deriving (Show)

newPointLight :: Tuple -> Color -> PointLight
newPointLight p c = MkPointLight { position = p, intensity = c }


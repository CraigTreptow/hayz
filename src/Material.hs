module Material ( Material (..)
                , defaultMaterial
                , lighting
                ) where

import Color as C
import Tuple as T
import Light

-- TODO enforce this
-- each is non-negative floating point
data Material = MkMaterial { color     :: Color
                           , ambient   :: Double
                           , diffuse   :: Double
                           , specular  :: Double
                           , shininess :: Double
                           } deriving (Show, Eq)

defaultMaterial :: Material
defaultMaterial = MkMaterial { color = (newColor 1 1 1), ambient = 0.1, diffuse = 0.9, specular = 0.9, shininess = 200.0 }

lighting :: Material -> PointLight -> Tuple -> Tuple -> Tuple -> Color
lighting material light point eyeV normalV = (ambientC `C.addedTo` diffuseC) `C.addedTo` specularC
        -- combine the surface color with the light's color/intensity
  where effectiveColor = (color material) `C.multipliedBy` (intensity light)
        -- find the direction to the light source
        lightV         = normalize (point `T.subtractedFrom` (position light))
        -- compute the ambient contribution
        ambientC       = effectiveColor `C.multipliedByS` (ambient material)
        -- light_dot_normal represents the cosine of the angle between the
        -- light vector and the normal vector. A negative number means the
        -- light is on the other side of the surface.
        lightDotNormal = dot lightV normalV
        colors         = computeDiffuseAndSpecular lightDotNormal effectiveColor material eyeV lightV normalV light
        diffuseC       = fst colors
        specularC      = snd colors


computeDiffuseAndSpecular :: Double -> Color -> Material -> Tuple -> Tuple -> Tuple -> PointLight -> (Color, Color)
computeDiffuseAndSpecular ldn effC mat eyeV lightV normalV light = (diffuse, specular)
  where diffuse  = computeDiffuse ldn effC mat
        specular = computeSpecular ldn eyeV lightV normalV mat light

computeDiffuse :: Double -> Color -> Material -> Color
computeDiffuse n c m =
  if n < 0
     then black
     else (c `C.multipliedByS` (diffuse m)) `C.multipliedByS` n
  where black = newColor 0 0 0

computeSpecular :: Double -> Tuple -> Tuple -> Tuple -> Material -> PointLight -> Color
computeSpecular n eyeV lightV normalV material light =
  if n < 0
     then black
     else specularC
  where black = newColor 0 0 0
        -- reflect_dot_eye represents the cosine of the angle between the
        -- reflection vector and the eye vector. A negative number means the
        -- light reflects away from the eye.
        reflectV = reflect (neg lightV) normalV
        reflectDotEye = dot reflectV eyeV
        factor = reflectDotEye ** (shininess material)
        specularC = (intensity light) `C.multipliedByS` (specular material) `C.multipliedByS` factor


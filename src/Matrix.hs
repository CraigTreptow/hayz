module Matrix ( Matrix (..)
              , matrixEq
              , equalEnough
              , Matrix.multipliedByTuple
              , subMatrix
              , minor
              , cofactor
              , determinant
              , isInvertible
              , invert
              , translation
              , scaling
              , shearing
              , rotationX
              , rotationY
              , rotationZ
              ) where

-- Matrix indexing is 1 based: row col

import Tuple
import Data.Matrix as M

rotationZ :: Double -> (Matrix Double)
rotationZ r = i''''
  where i = identity 4
        i' = setElem (cos r) (1,1) i
        i'' = setElem (-(sin r)) (1,2) i'
        i''' = setElem (sin r) (2,1) i''
        i'''' = setElem (cos r) (2,2) i'''

rotationY :: Double -> (Matrix Double)
rotationY r = i''''
  where i = identity 4
        i' = setElem (cos r) (1,1) i
        i'' = setElem (sin r) (1,3) i'
        i''' = setElem (-(sin r)) (3,1) i''
        i'''' = setElem (cos r) (3,3) i'''

rotationX :: Double -> (Matrix Double)
rotationX r = i''''
  where i = identity 4
        i' = setElem (cos r) (2,2) i
        i'' = setElem (-(sin r)) (2,3) i'
        i''' = setElem (sin r) (3,2) i''
        i'''' = setElem (cos r) (3,3) i'''

shearing :: Double -> Double -> Double -> Double -> Double -> Double -> (Matrix Double)
shearing xy xz yx yz zx zy = i''''''
  where i = identity 4
        i' = setElem xy (1,2) i
        i'' = setElem xz (1,3) i'
        i''' = setElem yx (2,1) i''
        i'''' = setElem yz (2,3) i'''
        i''''' = setElem zx (3,1) i''''
        i'''''' = setElem zy (3,2) i'''''

scaling :: Double -> Double -> Double -> (Matrix Double)
scaling x y z = i'''
  where i = identity 4
        i' = setElem x (1,1) i
        i'' = setElem y (2,2) i'
        i''' = setElem z (3,3) i''

translation :: Double -> Double -> Double -> (Matrix Double)
translation x y z = i'''
  where i = identity 4
        i' = setElem x (1,4) i
        i'' = setElem y (2,4) i'
        i''' = setElem z (3,4) i''

invert :: (Matrix Double) -> (Matrix Double)
invert m
  | (isInvertible m == False) = m
  | (isInvertible m == True) = case (inverse m) of
                                 Left s  -> m
                                 Right i -> i

isInvertible :: (Matrix Double) -> Bool
isInvertible m
  | (d == 0)  = False
  | otherwise = True
  where d = determinant m

cofactor :: Int -> Int -> (Matrix Double) -> Double
cofactor r c m
  | odd (r + c) = -(minor r c m)
  | otherwise   = minor r c m

minor :: Int -> Int -> (Matrix Double) -> Double
minor r c m = determinant (minorMatrix r c m)

subMatrix :: Int -> Int -> (Matrix Double) -> (Matrix Double)
subMatrix r c m = minorMatrix r c m

determinant :: (Matrix Double) -> Double
determinant m = detLU m

matrixEq m1 m2 = not (elem False results)
    where l1 = concat $ toLists m1
          l2 = concat $ toLists m2
          results = zipWith (equalEnough) l1 l2

equalEnough x y = abs(x - y) < epsilon
  where
    epsilon = 0.00001

multipliedByTuple :: (Matrix Double) -> Tuple -> Tuple
multipliedByTuple m t = newTuple newX newY newZ newW
  where
    newX = ((getElem 1 1 m) * (x t)) + ((getElem 1 2 m) * (y t)) + ((getElem 1 3 m) * (z t)) + ((getElem 1 4 m) * (w t))
    newY = ((getElem 2 1 m) * (x t)) + ((getElem 2 2 m) * (y t)) + ((getElem 2 3 m) * (z t)) + ((getElem 2 4 m) * (w t))
    newZ = ((getElem 3 1 m) * (x t)) + ((getElem 3 2 m) * (y t)) + ((getElem 3 3 m) * (z t)) + ((getElem 3 4 m) * (w t))
    newW = ((getElem 4 1 m) * (x t)) + ((getElem 4 2 m) * (y t)) + ((getElem 4 3 m) * (z t)) + ((getElem 4 4 m) * (w t))

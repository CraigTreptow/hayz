module Color ( Color (..)
             , R (..)
             , G (..)
             , B (..)
             , newColor
             , addedTo
             , subtractedFrom
             , multipliedByS
             , multipliedBy
             ) where

type R = Double
type G = Double
type B = Double

data Color = MkColor { red :: R
                     , green :: G
                     , blue :: B
                     } deriving (Show)

instance Eq Color where
  a == b =    abs(red a - red b)     < epsilon
           && abs(green a - green b) < epsilon
           && abs(blue a - blue b)   < epsilon
    where
      epsilon = 0.00001

multipliedByS :: Color -> Double -> Color
multipliedByS c s = newColor (red c * s) (green c * s) (blue c * s)

-- Hadamard or Schur product
multipliedBy :: Color -> Color -> Color
multipliedBy c1 c2 = newColor (red c1 * red c2) (green c1 * green c2) (blue c1 * blue c2)

subtractedFrom :: Color -> Color -> Color
subtractedFrom c2 c1 = newColor (red c1 - red c2) (green c1 - green c2) (blue c1 - blue c2)

addedTo :: Color -> Color -> Color
addedTo c1 c2 = newColor (red c1 + red c2) (green c1 + green c2) (blue c1 + blue c2)

newColor :: R -> G -> B -> Color
newColor r g b = MkColor { red = r, green = g, blue = b }


module Tuple ( Tuple (..)
             , newPoint
             , newVector
             , newTuple
             , isPoint
             , isVector
             , addedTo
             , subtractedFrom
             , neg
             , multipliedBy
             , dividedBy
             , magnitude
             , normalize
             , dot
             , cross
             , reflect
             ) where

data Tuple = MkTuple { x :: Double
                     , y :: Double
                     , z :: Double
                     , w :: Double
                     } deriving (Show)

instance Eq Tuple where
  a == b = abs(x a - x b) < epsilon
           && abs(y a - y b) < epsilon
           && abs(z a - z b) < epsilon
           && abs(w a - w b) < epsilon
    where
      epsilon = 0.00001

reflect :: Tuple -> Tuple -> Tuple
reflect incoming normal =
  ((normal `multipliedBy` 2) `multipliedBy` (dot incoming normal)) `subtractedFrom` incoming

cross :: Tuple -> Tuple -> Tuple
cross a b =
  if isVector a && isVector b then
    newVector newX newY newZ
  else
    error "cross is only for vectors!"

      where newX = (y a * z b) - (z a * y b)
            newY = (z a * x b) - (x a * z b)
            newZ = (x a * y b) - (y a * x b)

-- the smaller the dot product,
-- the larger the angle between the vectors
dot :: Tuple -> Tuple -> Double
dot v v' =
  if isVector v && isVector v' then
    (x v * x v') +
    (y v * y v') +
    (z v * z v') +
    (w v * w v')
  else
    error "dot is only for vectors!"

normalize :: Tuple -> Tuple
normalize v =
  newVector newX newY newZ
    where newX = (x v) / m
          newY = (y v) / m
          newZ = (z v) / m
          m = magnitude v

neg :: Tuple -> Tuple
neg t =
  newTuple newX newY newZ newW
    where
      newX = negate (x t)
      newY = negate (y t)
      newZ = negate (z t)
      newW = negate (w t)

magnitude :: Tuple -> Double
magnitude v =
  if isVector v then
   sqrt (
     (x v * x v) +
     (y v * y v) +
     (z v * z v) +
     (w v * w v)
   )
  else
    error "Magnitude is only for vectors!"

dividedBy :: Tuple -> Double -> Tuple
dividedBy t s = newTuple (x t / s) (y t / s) (z t / s) (w t / s)

multipliedBy :: Tuple -> Double -> Tuple
multipliedBy t s = newTuple (x t * s) (y t * s) (z t * s) (w t * s)

subtractedFrom :: Tuple -> Tuple -> Tuple
subtractedFrom t2 t1 = newTuple (x t1 - x t2) (y t1 - y t2) (z t1 - z t2) (w t1 - w t2)

addedTo :: Tuple -> Tuple -> Tuple
addedTo t1 t2 = newTuple (x t1 + x t2) (y t1 + y t2) (z t1 + z t2) (w t1 + w t2)

isPoint :: Tuple -> Bool
isPoint t = (w t) == 1.0

isVector :: Tuple -> Bool
isVector t = (w t) == 0

newPoint :: Double -> Double -> Double -> Tuple
newPoint x y z = MkTuple { x = x, y = y, z = z, w = 1.0 }

newVector :: Double -> Double -> Double -> Tuple
newVector x y z = MkTuple { x = x, y = y, z = z, w = 0 }

newTuple :: Double -> Double -> Double -> Double -> Tuple
newTuple x y z w = MkTuple { x = x, y = y, z = z, w = w }


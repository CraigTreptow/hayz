module LightSpec (spec) where

import Test.Hspec
-- import Test.QuickCheck
-- import Control.Exception (evaluate)

import Light
import Tuple
import Color

spec :: Spec
spec = do
  describe "Point Light" $ do
    it "a point light has a position and intensity" $ do
      let i = newColor 1 1 1
      let p = newPoint 0 0 0
      let light = newPointLight p i

      (position light) `shouldBe` p
      (intensity light) `shouldBe` i


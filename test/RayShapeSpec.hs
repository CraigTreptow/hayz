module RayShapeSpec (spec) where

import Test.Hspec
-- import Test.QuickCheck
-- import Control.Exception (evaluate)

import RayShape as R
import Material
import Light as L
import Tuple
import Color
import Matrix
import Data.Matrix
import System.Random

spec :: Spec
spec = do
  describe "Rays" $ do
    it "a tuple with w = 1.0 is a point" $ do
      let o = newPoint 1 2 3
      let d = newVector 4 5 6
      let r = ray o d

      (origin r) `shouldBe` o
      (direction r) `shouldBe` d

    it "can compute a point from a distance" $ do
      let r = ray (newPoint 2 3 4) (newVector 1 0 0)

      (R.position r 0) `shouldBe` (newPoint 2 3 4)
      (R.position r 1) `shouldBe` (newPoint 3 3 4)
      (R.position r (-1)) `shouldBe` (newPoint 1 3 4)
      (R.position r 2.5) `shouldBe` (newPoint 4.5 3 4)

    it "can translate a ray" $ do
      let r = ray (newPoint 1 2 3) (newVector 0 1 0)
      let m = translation 3 4 5
      let r2 = transform r m

      (origin r2) `shouldBe` (newPoint 4 6 8)
      (direction r2) `shouldBe` (newVector 0 1 0)

    it "can scale a ray" $ do
      let r = ray (newPoint 1 2 3) (newVector 0 1 0)
      let m = scaling 2 3 4
      let r2 = transform r m

      (origin r2) `shouldBe` (newPoint 2 6 12)
      (direction r2) `shouldBe` (newVector 0 3 0)

  describe "Shapes" $ do
    it "creates unique spheres" $ do
      seed <- randomRIO (1, maxBound :: Int)
      let s = newSphere seed
      let s' = newSphere (seed + 1)

      (s == s') `shouldBe` False

    it "encapsulates t values and an object in an intersection" $ do
      seed <- randomRIO (1, maxBound :: Int)
      let s = newSphere seed
      let i = MkIntersection 3.5 s

      tValue i `shouldBe` 3.5
      object i `shouldBe` s

    it "sets the object on an intersection" $ do
      seed <- randomRIO (1, maxBound :: Int)
      let r = ray (newPoint 0 0 (-5)) (newVector 0 0 1)
      let s = newSphere seed
      let xs = intersect s r

      length xs `shouldBe` 2
      object (head xs) `shouldBe` s
      object (head $ tail xs) `shouldBe` s

    it "aggregates intersections" $ do
      seed <- randomRIO (1, maxBound :: Int)
      let s = newSphere seed
      let i1 = MkIntersection 1 s
      let i2 = MkIntersection 2 s
      let xs = [i1, i2]

      length xs `shouldBe` 2
      (xs !! 0) `shouldBe` i1
      (xs !! 1) `shouldBe` i2

    it "intersects a ray and sphere at two points" $ do
      seed <- randomRIO (1, maxBound :: Int)
      let r = ray (newPoint 0 0 (-5)) (newVector 0 0 1)
      let s = newSphere seed
      let intersection = intersect s r

      (length intersection) `shouldBe` 2
      tValue (head intersection) `shouldBe` 4.0
      tValue (head $ tail intersection) `shouldBe` 6.0

    it "doesn't intersect a ray and sphere at all" $ do
      seed <- randomRIO (1, maxBound :: Int)
      let r = ray (newPoint 10 10 10) (newVector 0 0 1)
      let s = newSphere seed
      let intersection = intersect s r

      (length intersection) `shouldBe` 0

    it "intersects a ray that originates inside a sphere" $ do
      seed <- randomRIO (1, maxBound :: Int)
      let r = ray (newPoint 0 0 0) (newVector 0 0 1)
      let s = newSphere seed
      let intersection = intersect s r

      (length intersection) `shouldBe` 2
      tValue (head intersection) `shouldBe` (-1.0)
      tValue (head $ tail intersection) `shouldBe` 1.0

    it "intersects a ray and a sphere that is behind the ray" $ do
      seed <- randomRIO (1, maxBound :: Int)
      let r = ray (newPoint 0 0 5) (newVector 0 0 1)
      let s = newSphere seed
      let intersection = intersect s r

      (length intersection) `shouldBe` 2
      tValue (head intersection) `shouldBe` (-6.0)
      tValue (head $ tail intersection) `shouldBe` (-4.0)

    context "hits" $ do
      it "the hit with all intersections having a positive t value" $ do
        seed <- randomRIO (1, maxBound :: Int)
        let s = newSphere seed
        let i1 = MkIntersection 1 s
        let i2 = MkIntersection 2 s
        let xs = [i1, i2]

        let i = hit(xs)

        i `shouldBe` Just i1

      it "the hit when some t values are negative" $ do
        seed <- randomRIO (1, maxBound :: Int)
        let s = newSphere seed
        let i1 = MkIntersection (-1) s
        let i2 = MkIntersection 1 s
        let xs = [i2, i1]

        let i = hit(xs)

        i `shouldBe` Just i2

      it "the hit with all intersections having a negative t value" $ do
        seed <- randomRIO (1, maxBound :: Int)
        let s = newSphere seed
        let i1 = MkIntersection (-2) s
        let i2 = MkIntersection (-1) s
        let xs = [i2, i1]

        let i = hit(xs)

        i `shouldBe` Nothing

      it "the hit is the lowest non-negative intersection" $ do
        seed <- randomRIO (1, maxBound :: Int)
        let s = newSphere seed
        let i1 = MkIntersection 5 s
        let i2 = MkIntersection 7 s
        let i3 = MkIntersection (-3) s
        let i4 = MkIntersection 2 s
        let xs = [i1, i2, i3, i4]

        let i = hit(xs)

        i `shouldBe` Just i4

      it "makes spheres with a default transofmration" $ do
        seed <- randomRIO (1, maxBound :: Int)
        let s = newSphere seed

        (transformation s) `shouldBe` (identity 4)

      it "can change a sphere's transofrmation" $ do
        seed <- randomRIO (1, maxBound :: Int)
        let s = newSphere seed
        let t = translation 2 3 4
        let s' = setTransformation s t

        (transformation s') `shouldBe` t

      it "can intersect a scaled sphere with a ray" $ do
        seed <- randomRIO (1, maxBound :: Int)
        let r = ray (newPoint 0 0 (-5)) (newVector 0 0 1)
        let s = newSphere seed
        let s' = setTransformation s (scaling 2 2 2)
        let xs = intersect s' r

        length xs `shouldBe` 2
        tValue (head xs) `shouldBe` 3
        tValue (head $ tail xs) `shouldBe` 7

      it "can intersect a translated sphere with a ray" $ do
        seed <- randomRIO (1, maxBound :: Int)
        let r = ray (newPoint 0 0 (-5)) (newVector 0 0 1)
        let s = newSphere seed
        let s' = setTransformation s (translation 5 0 0)
        let xs = intersect s' r

        length xs `shouldBe` 0

    context "light and shading" $ do
      it "the normal on a sphere at a point on the x axis" $ do
        seed <- randomRIO (1, maxBound :: Int)
        let s = newSphere seed
        let n = normalAt s (newPoint 1 0 0)

        n `shouldBe` (newVector 1 0 0)

      it "the normal on a sphere at a point on the y axis" $ do
        seed <- randomRIO (1, maxBound :: Int)
        let s = newSphere seed
        let n = normalAt s (newPoint 0 1 0)

        n `shouldBe` (newVector 0 1 0)

      it "the normal on a sphere at a point on the z axis" $ do
        seed <- randomRIO (1, maxBound :: Int)
        let s = newSphere seed
        let n = normalAt s (newPoint 0 0 1)

        n `shouldBe` (newVector 0 0 1)

      it "the normal on a sphere at a nonaxial point" $ do
        seed <- randomRIO (1, maxBound :: Int)
        let s = newSphere seed
        let sqrt3 = sqrt 3
        let n = normalAt s (newPoint (sqrt3 / 3) (sqrt3 / 3) (sqrt3 / 3))

        n `shouldBe` (newVector (sqrt3 / 3) (sqrt3 / 3) (sqrt3 / 3))

      it "the normal is a normalized vector" $ do
        seed <- randomRIO (1, maxBound :: Int)
        let s = newSphere seed
        let sqrt3 = sqrt 3
        let n = normalAt s (newPoint (sqrt3 / 3) (sqrt3 / 3) (sqrt3 / 3))

        n `shouldBe` normalize n

      it "computing the normal on a translated sphere" $ do
        seed <- randomRIO (1, maxBound :: Int)
        let s = newSphere seed
        let s' = setTransformation s (translation 0 1 0)
        let n = normalAt s' (newPoint 0 1.70711 (-0.70711))

        n `shouldBe` newVector 0 0.70711 (-0.70711)

      it "computing the normal on a transformed sphere" $ do
        seed <- randomRIO (1, maxBound :: Int)
        let s = newSphere seed
        let sqrt2 = sqrt 2
        let m = (scaling 1 0.5 1) * (rotationZ (pi/5))
        let s' = setTransformation s m
        let n = normalAt s' (newPoint 0 (sqrt2 / 2) (-(sqrt2 / 2)))

        n `shouldBe` newVector 0 0.97014 (-0.24254)

    context "material" $ do
      it "has spheres with default materials" $ do
        seed <- randomRIO (1, maxBound :: Int)
        let s = newSphere seed
        let mat = material s

        mat `shouldBe` defaultMaterial

      it "can assign materials to spheres" $ do
        seed <- randomRIO (1, maxBound :: Int)
        let s = newSphere seed
        let mat = defaultMaterial { ambient = 1.0 }
        let s' = setMaterial s mat

        (material s') `shouldBe` mat

    context "lighting" $ do
      let mat = defaultMaterial
      let pos = newPoint 0 0 0

      it "lights with the eye between the light and the surface" $ do
        let eyeV = newVector 0 0 (-1)
        let normalV = newVector 0 0 (-1)
        let p = newPoint 0 0 (-10)
        let c = newColor 1 1 1
        let light = newPointLight p c
        let result = lighting mat light pos eyeV normalV

        result `shouldBe` newColor 1.9 1.9 1.9

      it "lights with the eye between the light and the surface, eye offset 45 degrees" $ do
        let sr2 = sqrt 2
        let eyeV = newVector 0 (sr2/2) (-(sr2/2))
        let normalV = newVector 0 0 (-1)
        let p = newPoint 0 0 (-10)
        let c = newColor 1 1 1
        let light = newPointLight p c
        let result = lighting mat light pos eyeV normalV

        result `shouldBe` newColor 1.0 1.0 1.0

      it "lights with the eye between the light and the surface, light offset 45 degrees" $ do
        let eyeV = newVector 0 0 (-1)
        let normalV = newVector 0 0 (-1)
        let p = newPoint 0 10 (-10)
        let c = newColor 1 1 1
        let light = newPointLight p c
        let result = lighting mat light pos eyeV normalV

        result `shouldBe` newColor 0.7364 0.7364 0.7364

      it "lights with the eye in the path of the reflection vector" $ do
        let sr2 = sqrt 2
        let eyeV = newVector 0 (-(sr2/2)) (-(sr2/2))
        let normalV = newVector 0 0 (-1)
        let p = newPoint 0 10 (-10)
        let c = newColor 1 1 1
        let light = newPointLight p c
        let result = lighting mat light pos eyeV normalV

        result `shouldBe` newColor 1.6364 1.6364 1.6364

      it "lights with light behind the surface" $ do
        let eyeV = newVector 0 0 (-1)
        let normalV = newVector 0 0 (-1)
        let p = newPoint 0 0 10
        let c = newColor 1 1 1
        let light = newPointLight p c
        let result = lighting mat light pos eyeV normalV

        result `shouldBe` newColor 0.1 0.1 0.1


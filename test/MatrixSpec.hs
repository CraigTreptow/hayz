module MatrixSpec (spec) where

import Test.Hspec
-- import Test.QuickCheck
-- import Control.Exception (evaluate)

import Matrix
import Tuple
import Data.Matrix

spec :: Spec
spec = do
  describe "Matrix" $ do
    it "can create and inspect a 4x4 matrix" $ do
      let l = [ [ 1,       2,    3, 4   ]
              , [ 5.5,   6.5,  7.5, 8.5 ]
              , [ 9,      10,   11, 12  ]
              , [ 13.5, 14.5, 15.5, 16.5] ]
      let m = fromLists l

      (getElem 1 1 m) `shouldBe` 1
      (getElem 1 4 m) `shouldBe` 4
      (getElem 2 1 m) `shouldBe` 5.5
      (getElem 2 3 m) `shouldBe` 7.5
      (getElem 3 3 m) `shouldBe` 11
      (getElem 4 1 m) `shouldBe` 13.5
      (getElem 4 3 m) `shouldBe` 15.5

    it "can create and inspect a 3x3 matrix" $ do
      let l = [ [ -3,  5,  0 ]
              , [  1, -2, -7 ]
              , [  0,  1,  1 ] ]
      let m = fromLists l

      (getElem 1 1 m) `shouldBe` -3
      (getElem 2 2 m) `shouldBe` -2
      (getElem 3 3 m) `shouldBe` 1

    it "can create and inspect a 2x2 matrix" $ do
      let l = [ [ -3, 5 ]
              , [ 1, -2 ] ]
      let m = fromLists l

      (getElem 1 1 m) `shouldBe` -3
      (getElem 1 2 m) `shouldBe` 5
      (getElem 2 1 m) `shouldBe` 1
      (getElem 2 2 m) `shouldBe` -2

    it "can compare two matrices" $ do
      let l = [ [ 1,       2,    3, 4   ]
              , [ 5.5,   6.5,  7.5, 8.5 ]
              , [ 9,      10,   11, 12  ]
              , [ 13.5, 14.5, 15.5, 16.5] ]
      let m = fromLists l
      let m' = fromLists l

      (matrixEq m m') `shouldBe` True

    it "matrices with minute differences are equal" $ do
      let l  = [ [ 1.000006, 2  ]
               , [ 1.00005,  6.5] ]
      let l' = [ [ 1.000007, 2  ]
               , [ 1.00005,  6.5] ]
      let m = fromLists l
      let m' = fromLists l'

      (matrixEq m m') `shouldBe` True

    it "can multiply two matrices" $ do
      let l  = [ [ 1, 2, 3, 4 ]
               , [ 5, 6, 7, 8 ]
               , [ 9, 8, 7, 6 ]
               , [ 5, 4, 3, 2 ] ]

      let l' = [ [ -2, 1, 2,  3 ]
               , [  3, 2, 1, -1 ]
               , [  4, 3, 6,  5 ]
               , [  1, 2, 7,  8 ] ]

      let m  = fromLists l
      let m' = fromLists l'

      let result = m * m'
      let expectedL = [ [ 20, 22,  50,  48 ]
                      , [ 44, 54, 114, 108 ]
                      , [ 40, 58, 110, 102 ]
                      , [ 16, 26,  46,  42 ] ]
      let expected = fromLists expectedL

      (matrixEq result expected) `shouldBe` True

    it "can multiply a matrix and a tuple" $ do
      let l  = [ [ 1, 2, 3, 4 ]
               , [ 2, 4, 4, 2 ]
               , [ 8, 6, 4, 1 ]
               , [ 0, 0, 0, 1 ] ]

      let m  = fromLists l
      let t = newTuple 1 2 3 1

      let result = m `Matrix.multipliedByTuple` t
      let expected = newTuple 18.0 24.0 33.0 1.0

      result `shouldBe` expected

    it "can multiply a matrix by the identiy matrix" $ do
      let l  = [ [ 0, 1, 2,  4 ]
               , [ 1, 2, 4,  8 ]
               , [ 2, 4, 8, 16 ]
               , [ 4, 8, 16,32 ] ]

      let m  = fromLists l
      let i = identity 4

      let result = m * i

      (matrixEq result m) `shouldBe` True

    it "can multiply the identity matrix and a tuple" $ do
      let i = identity 4
      let t = newTuple 1 2 3 1

      let result = i `Matrix.multipliedByTuple` t
      let expected = t

      result `shouldBe` expected

    it "can transpose a matrix" $ do
      let l  = [ [ 0, 9, 3, 0 ]
               , [ 9, 8, 0, 8 ]
               , [ 1, 8, 5, 3 ]
               , [ 0, 0, 5, 8 ] ]

      let m  = fromLists l

      let result = transpose m
      let expectedL = [ [ 0, 9, 1, 0 ]
                      , [ 9, 8, 8, 0 ]
                      , [ 3, 0, 5, 5 ]
                      , [ 0, 8, 3, 8 ] ]
      let expected = fromLists expectedL

      (matrixEq result expected) `shouldBe` True

    it "can transpose the identity matrix" $ do
      let i = identity 4
      let result = transpose i

      (matrixEq result i) `shouldBe` True

    it "can calculate the determinant of a 2x2 matrix" $ do
      let l = [ [  1, 5 ]
              , [ -3, 2 ] ]
      let m = fromLists l

      (determinant m) `shouldBe` 17

    it "a submatrix of a 3x3 matrix is a 2x2 matrix" $ do
      let l  = [ [  1, 5,  0 ]
               , [ -3, 2,  7 ]
               , [  0, 6, -3 ] ]

      let m  = fromLists l

      let result = subMatrix 1 3 m
      let expectedL = [ [ -3, 2 ]
                      , [  0, 6 ] ]
      let expected = fromLists expectedL

      (matrixEq result expected) `shouldBe` True

    it "a submatrix of a 4x4 matrix is a 3x3 matrix" $ do
      let l  = [ [ -6, 1,  1, 6 ]
               , [ -8, 5,  8, 6 ]
               , [ -1, 0,  8, 2 ]
               , [ -7, 1, -1, 1 ] ]

      let m  = fromLists l

      let result = subMatrix 3 2 m
      let expectedL = [ [ -6,  1, 6 ]
                      , [ -8,  8, 6 ]
                      , [ -7, -1, 1 ] ]

      let expected = fromLists expectedL

      (matrixEq result expected) `shouldBe` True

    it "can calculate the minor of a 3x3 matrix" $ do
      let l = [ [ 3,  5,  0 ]
              , [ 2, -1, -7 ]
              , [ 6, -1,  5 ] ]

      let a = fromLists l
      let b = subMatrix 2 1 a

      (determinant b) `shouldBe` 25
      (minor 2 1 a) `shouldBe` 25

    it "can calculate the cofactor of a 3x3 matrix" $ do
      let l = [ [ 3,  5,  0 ]
              , [ 2, -1, -7 ]
              , [ 6, -1,  5 ] ]

      let a = fromLists l

      (minor 1 1 a) `shouldBe` -12
      (cofactor 1 1 a) `shouldBe` -12

      (minor 2 1 a) `shouldBe` 25
      (cofactor 2 1 a) `shouldBe` -25

    it "can calculate the determinant of a 3x3 matrix" $ do
      let l = [ [  1, 2,  6 ]
              , [ -5, 8, -4 ]
              , [  2, 6,  4 ] ]

      let a = fromLists l

      (cofactor 1 1 a) `shouldBe` 56
      (cofactor 1 2 a) `shouldBe` 12
      (cofactor 1 3 a) `shouldBe` -46
      (determinant a) `shouldBe` -196

    it "can calculate the determinant of a 4x4 matrix" $ do
      let l = [ [ -2, -8,  3,  5 ]
              , [ -3,  1,  7,  3 ]
              , [  1,  2, -9,  6 ]
              , [ -6,  7,  7, -9 ] ]

      let a = fromLists l

      (cofactor 1 1 a) `shouldBe` 690
      (cofactor 1 2 a) `shouldBe` 447
      (cofactor 1 3 a) `shouldBe` 210
      -- TODO this round may bite later
      round (cofactor 1 4 a) `shouldBe` 51
      round (determinant a) `shouldBe` -4071

    it "can test for invertibility and be True" $ do
      let l = [ [ 6,  4, 4,  4 ]
              , [ 5,  5, 7,  6 ]
              , [ 4, -9, 3, -7 ]
              , [ 9,  1, 7, -6 ] ]

      let a = fromLists l

      round (determinant a) `shouldBe` -2120
      isInvertible a `shouldBe` True

    it "can test for invertibility and be False" $ do
      let l = [ [ -4,  2, -2, -3 ]
              , [  9,  6,  2,  6 ]
              , [  0, -5,  1, -5 ]
              , [  0,  0,  0,  0 ] ]

      let a = fromLists l

      round (determinant a) `shouldBe` 0
      isInvertible a `shouldBe` False

    it "can calculate the inverse of a 4x4 matrix" $ do
      let l = [ [ -5,  2,  6, -8 ]
              , [  1, -5,  1,  8 ]
              , [  7,  7, -6, -7 ]
              , [  1, -3,  7,  4 ] ]

      let a = fromLists l
      let b = invert a

      round (determinant a) `shouldBe` 532
      round (cofactor 3 4 a) `shouldBe` -160
      equalEnough (getElem 4 3 b) (-160/532) `shouldBe` True
      round (cofactor 4 3 a) `shouldBe` 105
      equalEnough (getElem 3 4 b) (105/532) `shouldBe` True

      let expectedL = [ [  0.21805,  0.45113,  0.24060, -0.04511 ]
                      , [ -0.80827, -1.45677, -0.44361,  0.52068 ]
                      , [ -0.07895, -0.22368, -0.05263,  0.19737 ]
                      , [ -0.52256, -0.81391, -0.30075,  0.30639 ] ]
      let expectedB = fromLists expectedL
      (matrixEq b expectedB) `shouldBe` True

    it "can calculate the inverse of another 4x4 matrix" $ do
      let l = [ [  8, -5,  9,  2 ]
              , [  7,  5,  6,  1 ]
              , [ -6,  0,  9,  6 ]
              , [ -3,  0, -9, -4 ] ]

      let a = fromLists l
      let b = invert a

      let expectedL = [ [ -0.15385, -0.15385, -0.28205, -0.53846 ]
                      , [ -0.07692,  0.12308,  0.02564,  0.03077 ]
                      , [  0.35897,  0.35897,  0.43590,  0.92308 ]
                      , [ -0.69231, -0.69231, -0.76923, -1.92308 ] ]

      let expectedB = fromLists expectedL
      (matrixEq b expectedB) `shouldBe` True

    it "can calculate the inverse of a third 4x4 matrix" $ do
      let l = [ [  9,  3,  0,  9 ]
              , [ -5, -2, -6, -3 ]
              , [ -4,  9,  6,  4 ]
              , [ -7,  6,  6,  2 ] ]

      let a = fromLists l
      let b = invert a

      let expectedL = [ [ -0.04074, -0.07778,  0.14444, -0.22222 ] 
                      , [ -0.07778,  0.03333,  0.36667, -0.33333 ] 
                      , [ -0.02901, -0.14630, -0.10926,  0.12963 ] 
                      , [  0.17778,  0.06667, -0.26667,  0.33333 ] ]

      let expectedB = fromLists expectedL
      (matrixEq b expectedB) `shouldBe` True

    it "can multiply a product by its inverse" $ do
      let la = [ [  3, -9,  7,  3 ]
               , [  3, -8,  2, -9 ]
               , [ -4,  4,  4,  1 ]
               , [ -6,  5, -1,  1 ] ]

      let a = fromLists la

      let lb = [ [  8,  2, 2, 2 ]
               , [  3, -1, 7, 0 ]
               , [  7,  0, 5, 4 ]
               , [  6, -2, 0, 5 ] ]

      let b = fromLists lb
      let c = a * b
      let d = c * (invert b)

      (matrixEq d a) `shouldBe` True

    it "can multiply by a translation matrix" $ do
      let transform = translation 5 (-3) 2
      let p = newPoint (-3) 4 5
      let result = (transform `Matrix.multipliedByTuple` p)
      let expected = newPoint 2 1 7

      result `shouldBe` expected

    it "can multiply by the inverse of a translation matrix" $ do
      let transform = translation 5 (-3) 2
      let inv = invert transform
      let p = newPoint (-3) 4 5
      let result = (inv `Matrix.multipliedByTuple` p)
      let expected = newPoint (-8) 7 3

      result `shouldBe` expected

    it "cannot modify a vector with a translation" $ do
      let transform = translation 5 (-3) 2
      let v = newVector (-3) 4 5
      let result = transform `Matrix.multipliedByTuple` v

      result `shouldBe` v

    it "can scale a point with a scaling matrix" $ do
      let transform = scaling 2 3 4
      let p = newPoint (-4) 6 8
      let result = (transform `Matrix.multipliedByTuple` p)
      let expected = newPoint (-8) 18 32

      result `shouldBe` expected

    it "can scale a vector with a scaling matrix" $ do
      let transform = scaling 2 3 4
      let v = newVector (-4) 6 8
      let result = (transform `Matrix.multipliedByTuple` v)
      let expected = newVector (-8) 18 32

      result `shouldBe` expected

    it "can scale with the inverse of a scaling matrix" $ do
      let transform = scaling 2 3 4
      let inv = invert transform
      let v = newVector (-4) 6 8
      let result = (inv `Matrix.multipliedByTuple` v)
      let expected = newVector (-2) 2 2

      result `shouldBe` expected

    it "can rotate a point around the x axis" $ do
      let p = newPoint 0 1 0
      let halfQuarter = rotationX(pi/4)
      let fullQuarter = rotationX(pi/2)

      let resultHalf = (halfQuarter `Matrix.multipliedByTuple` p)
      let resultFull = (fullQuarter `Matrix.multipliedByTuple` p)

      let expectedHalf = newPoint 0 ((sqrt 2) / 2) ((sqrt 2) / 2)
      let expectedFull = newPoint 0 0 1

      resultHalf `shouldBe` expectedHalf
      resultFull `shouldBe` expectedFull

    it "can rotate the opposite diretion with an inverse" $ do
      let p = newPoint 0 1 0
      let halfQuarter = rotationX(pi/4)
      let inv = invert halfQuarter

      let resultInv = (inv `Matrix.multipliedByTuple` p)

      let expectedInv = newPoint 0 ((sqrt 2) / 2) (-((sqrt 2) / 2))

      resultInv `shouldBe` expectedInv

    it "can rotate a point around the y axis" $ do
      let p = newPoint 0 0 1
      let halfQuarter = rotationY(pi/4)
      let fullQuarter = rotationY(pi/2)

      let resultHalf = (halfQuarter `Matrix.multipliedByTuple` p)
      let resultFull = (fullQuarter `Matrix.multipliedByTuple` p)

      let expectedHalf = newPoint ((sqrt 2) / 2) 0 ((sqrt 2) / 2)
      let expectedFull = newPoint 1 0 0

      resultHalf `shouldBe` expectedHalf
      resultFull `shouldBe` expectedFull

    it "can rotate a point around the z axis" $ do
      let p = newPoint 0 1 0
      let halfQuarter = rotationZ(pi/4)
      let fullQuarter = rotationZ(pi/2)

      let resultHalf = (halfQuarter `Matrix.multipliedByTuple` p)
      let resultFull = (fullQuarter `Matrix.multipliedByTuple` p)

      let expectedHalf = newPoint (-((sqrt 2) / 2)) ((sqrt 2) / 2) 0
      let expectedFull = newPoint (-1) 0 0

      resultHalf `shouldBe` expectedHalf
      resultFull `shouldBe` expectedFull

    it "can transform x in proportion to y via shearing" $ do
      let transform = shearing 1 0 0 0 0 0
      let p = newPoint 2 3 4
      let result = (transform `Matrix.multipliedByTuple` p)
      let expected = newPoint 5 3 4

      result `shouldBe` expected

    it "can transform x in proportion to z via shearing" $ do
      let transform = shearing 0 1 0 0 0 0
      let p = newPoint 2 3 4
      let result = (transform `Matrix.multipliedByTuple` p)
      let expected = newPoint 6 3 4

      result `shouldBe` expected

    it "can transform y in proportion to x via shearing" $ do
      let transform = shearing 0 0 1 0 0 0
      let p = newPoint 2 3 4
      let result = (transform `Matrix.multipliedByTuple` p)
      let expected = newPoint 2 5 4

      result `shouldBe` expected

    it "can transform y in proportion to z via shearing" $ do
      let transform = shearing 0 0 0 1 0 0
      let p = newPoint 2 3 4
      let result = (transform `Matrix.multipliedByTuple` p)
      let expected = newPoint 2 7 4

      result `shouldBe` expected

    it "can transform z in proportion to x via shearing" $ do
      let transform = shearing 0 0 0 0 1 0
      let p = newPoint 2 3 4
      let result = (transform `Matrix.multipliedByTuple` p)
      let expected = newPoint 2 3 6

      result `shouldBe` expected

    it "can transform z in proportion to y via shearing" $ do
      let transform = shearing 0 0 0 0 0 1
      let p = newPoint 2 3 4
      let result = (transform `Matrix.multipliedByTuple` p)
      let expected = newPoint 2 3 7

      result `shouldBe` expected

    it "applies individual transformations in sequence" $ do
      let p = newPoint 1 0 1
      let a = rotationX(pi/2)
      let b = scaling 5 5 5
      let c = translation 10 5 7

      let p2 = (a `Matrix.multipliedByTuple` p)
      p2 `shouldBe` (newPoint 1 (-1) 0)

      let p3 = (b `Matrix.multipliedByTuple` p2)
      p3 `shouldBe` (newPoint 5 (-5) 0)

      let p4 = (c `Matrix.multipliedByTuple` p3)
      p4 `shouldBe` (newPoint 15 0 7)

    it "applies chained transformations in reverse order" $ do
      let p = newPoint 1 0 1
      let a = rotationX(pi/2)
      let b = scaling 5 5 5
      let c = translation 10 5 7
      let t = c * b * a

      let result = (t `Matrix.multipliedByTuple` p)
      result `shouldBe` (newPoint 15 0 7)

    it "works for 3 o'clock like described in the book - clock chapter" $ do
      let twelve = newPoint 0 0 1
      let rotation = rotationY(3 * (pi/6))

      let maybeThree = (rotation `Matrix.multipliedByTuple` twelve)
      maybeThree `shouldBe` (newPoint 1 0 0)


module CanvasSpec (spec) where

import Test.Hspec
-- import Test.QuickCheck
-- import Control.Exception (evaluate)

import Canvas
import Color
import Data.String.Utils

spec :: Spec
spec = do
  describe "Canvas" $ do
    let black = newColor 0 0 0
    it "can create a canvas" $ do
      let c = newCanvas 10 20 black
      width c `shouldBe` 10
      height c `shouldBe` 20

    it "creates a canvas with all pixels black" $ do
      let c = newCanvas 2 2 black
      pixelAt c 0 0 `shouldBe` black
      pixelAt c 0 1 `shouldBe` black
      pixelAt c 1 0 `shouldBe` black
      pixelAt c 1 1 `shouldBe` black

    it "can create a PPM header" $ do
      let c = newCanvas 5 3 black
      let ppm = canvasToPPM c
      let l = lines ppm
      (l !! 0) `shouldBe` "P3"
      (l !! 1) `shouldBe` "5 3"
      (l !! 2) `shouldBe` "255"

    it "can create a PPM footer" $ do
      let c = newCanvas 5 3 black
      let ppm = canvasToPPM c
      endswith "\n" ppm `shouldBe` True

    it "can create PPM pixel data" $ do
      let c = newCanvas 5 3 black
      let c1 = newColor 1.5 0 0
      let c2 = newColor 0 (0.5) 0
      let c3 = newColor (-0.5) 0 1
      let c'   = writePixel c 0 0 c1
      let c''  = writePixel c' 2 1 c2
      let c''' = writePixel c'' 4 2 c3
      let ppm = canvasToPPM c'''
      let l = lines ppm
      (l !! 3) `shouldBe` "255 0 0 0 0 0 0 0 0 0 0 0 0 0 0"
      (l !! 4) `shouldBe` "0 0 0 0 0 0 0 128 0 0 0 0 0 0 0"
      (l !! 5) `shouldBe` "0 0 0 0 0 0 0 0 0 0 0 0 0 0 255"

    it "can split long lines in PPM files" $ do
      let c1 = newColor 1 0.8 0.6
      let c = newCanvas 10 2 c1
      let ppm = canvasToPPM c
      let l = lines ppm
      (l !! 3) `shouldBe` "255 205 154 255 205 154 255 205 154 255 205 154 255 205 154 255 205 154 255 205 154 255 205 154 255 205 154 255 205 154"
      (l !! 4) `shouldBe` "255 205 154 255 205 154 255 205 154 255 205 154 255 205 154 255 205 154 255 205 154 255 205 154 255 205 154 255 205 154"


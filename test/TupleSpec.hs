module TupleSpec (spec) where

import Test.Hspec
-- import Test.QuickCheck
-- import Control.Exception (evaluate)

import Tuple

spec :: Spec
spec = do
  describe "Tuples" $ do
    it "a tuple with w = 1.0 is a point" $ do
      let a = newTuple 4.3 (-4.2) 3.1 1
      isPoint a `shouldBe` True

    it "a tuple with w = 1.0 is not a vector" $ do
      let a = newTuple 4.3 (-4.2) 3.1 1
      isVector a `shouldNotBe` True

    it "a tuple with w = 1.0 has the correct values" $ do
      let a = newTuple 4.3 (-4.2) 3.1 1
      x a  `shouldBe` 4.3
      y a  `shouldBe` -4.2
      z a  `shouldBe` 3.1
      w a  `shouldBe` 1.0

    it "a tuple with w = 0.0 is a vector" $ do
      let a = newTuple 4.3 (-4.2) 3.1 0
      isVector a `shouldBe` True

    it "a tuple with w = 0.0 is not a point" $ do
      let a = newTuple 4.3 (-4.2) 3.1 0
      isPoint a `shouldNotBe` True

    it "a tuple with w = 0 has the correct values" $ do
      let a = newTuple 4.3 (-4.2) 3.1 0
      x a  `shouldBe` 4.3
      y a  `shouldBe` -4.2
      z a  `shouldBe` 3.1
      w a  `shouldBe` 0

  context "points" $ do
    it "creates tuples with w = 1.0" $ do
      let a = newPoint 4.3 (-4.2) 3.1
      w a  `shouldBe` 1.0

  context "vectors" $ do
    it "creates tuples with w = 0" $ do
      let a = newVector 4.3 (-4.2) 3.1
      w a  `shouldBe` 0

  context "add" $ do
    it "can add two tuples" $ do
      let a1 = newTuple 3 (-2) 5 1
      let a2 = newTuple (-2) 3 1 0
      let expected = newTuple 1 1 6 1
      a1 `addedTo` a2 `shouldBe` expected

  context "subtract" $ do
    it "can subtract two points" $ do
      let p1 = newPoint 3 2 1
      let p2 = newPoint 5 6 7
      let expected = newVector (-2) (-4) (-6)
      p2 `subtractedFrom` p1 `shouldBe` expected

    it "can subtract a vector from a point" $ do
      let p = newPoint 3 2 1
      let v = newVector 5 6 7
      let expected = newPoint (-2) (-4) (-6)
      v `subtractedFrom` p `shouldBe` expected

    it "can subtract two vectors" $ do
      let v1 = newVector 3 2 1
      let v2 = newVector 5 6 7
      let expected = newVector (-2) (-4) (-6)
      v2 `subtractedFrom` v1 `shouldBe` expected

    it "can subtract a vector from the zero vector" $ do
      let zero = newVector 0 0 0
      let v = newVector 1 (-2) 3
      let expected = newVector (-1) 2 (-3)
      v `subtractedFrom` zero `shouldBe` expected

  context "negate" $ do
    it "can negate a tuple" $ do
      let a = newTuple 1 (-2) 3 (-4)
      let expected = newTuple (-1) 2 (-3) 4
      neg a `shouldBe` expected

  context "multiply" $ do
    it "can multiply a tuple by a scalar" $ do
      let a = newTuple 1 (-2) 3 (-4)
      let expected = newTuple 3.5 (-7) 10.5 (-14)
      a `multipliedBy` 3.5 `shouldBe` expected

    it "can multiply a tuple by a fraction" $ do
      let a = newTuple 1 (-2) 3 (-4)
      let expected = newTuple 0.5 (-1) 1.5 (-2)
      a `multipliedBy` 0.5 `shouldBe` expected

  context "division" $ do
    it "can divide a tuple by a scalar" $ do
      let a = newTuple 1 (-2) 3 (-4)
      let expected = newTuple 0.5 (-1) 1.5 (-2)
      a `dividedBy` 2 `shouldBe` expected

  context "magnitude" $ do
    it "is correct for a v(1 0 0)" $ do
      let v = newVector 1 0 0
      magnitude v `shouldBe` 1

    it "is correct for a v(0 1 0)" $ do
      let v = newVector 0 1 0
      magnitude v `shouldBe` 1

    it "is correct for a v(0 0 1)" $ do
      let v = newVector 0 0 1
      magnitude v `shouldBe` 1

    it "is correct for a v(1 2 3)" $ do
      let v = newVector 1 2 3
      let expected = sqrt 14
      magnitude v `shouldBe` expected

    it "is correct for a v(-1 -2 -3)" $ do
      let v = newVector (-1) (-2) (-3)
      let expected = sqrt 14
      magnitude v `shouldBe` expected

    -- TODO get exception test working
    -- it "errors when not a vector p(1 2 3)" $ do
    --   let p = newPoint 1 2 3
    --   magnitude p `shouldThrow` "Magnitude is only for vectors"

  context "normalization" $ do
    it "is correct for a v(4 0 0)" $ do
      let v = newVector 4 0 0
      let expected = newVector 1 0 0
      normalize v `shouldBe` expected

    it "is correct for a v(1 2 3)" $ do
      let v = newVector 1 2 3
      -- vector 1/sqrt 14 2/sqrt 14 3/sqrt 14
      let expected = newVector 0.26726 0.53452 0.80178
      normalize v `shouldBe` expected

    it "has the correct magnitude for normalized vector v(1 2 3)" $ do
      let v = newVector 1 2 3
      let norm = normalize v
      magnitude norm `shouldBe` 1

  context "dot product" $ do
    it "is correct for a v(1 2 3) and v(2 3 4)" $ do
      let a = newVector 1 2 3
      let b = newVector 2 3 4
      dot a b `shouldBe` 20

  context "cross product" $ do
    it "is correct for a v(1 2 3) and v(2 3 4)" $ do
      let a = newVector 1 2 3
      let b = newVector 2 3 4
      let expected_ab = newVector (-1) 2 (-1)
      let expected_ba = newVector 1 (-2) 1
      cross a b `shouldBe` expected_ab
      cross b a `shouldBe` expected_ba

    -- TODO get exception test working
    -- it "errors when not a vector p(1 2 3)" $ do
    --   let p = newPoint 1 2 3
    --   magnitude p `shouldThrow` "Magnitude is only for vectors"

  context "reflection" $ do
    it "reflects a vector approaching at 45 degrees" $ do
      let v = newVector 1 (-1) 0
      let n = newVector 0 1 0

      (reflect v n)  `shouldBe` (newVector 1 1 0)

    it "reflects a vector off of a slanted surface" $ do
      let v = newVector 0 (-1) 0
      let sqrt2 = sqrt 2
      let n = newVector (sqrt2 / 2) (sqrt2 / 2) 0

      (reflect v n)  `shouldBe` (newVector 1 0 0)


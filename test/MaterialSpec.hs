module MaterialSpec (spec) where

import Test.Hspec
-- import Test.QuickCheck
-- import Control.Exception (evaluate)

import Material
import Color

spec :: Spec
spec = do
  describe "Materials" $ do
    it "has a default material" $ do
      -- let mat = defaultMaterial
      let mat = defaultMaterial
      color     mat `shouldBe` newColor 1 1 1
      ambient   mat `shouldBe` 0.1
      diffuse   mat `shouldBe` 0.9
      specular  mat `shouldBe` 0.9
      shininess mat `shouldBe` 200.0


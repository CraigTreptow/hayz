module ColorSpec (spec) where

import Test.Hspec
-- import Test.QuickCheck
-- import Control.Exception (evaluate)

import Color

spec :: Spec
spec = do
  describe "Colors" $ do
    it "can get the values from a color" $ do
      let c = newColor (-0.5) 0.4 1.7
      red   c  `shouldBe` -0.5
      green c  `shouldBe` 0.4
      blue  c  `shouldBe` 1.7

    it "can add colors" $ do
      let c1 = newColor 0.9 0.6 0.75
      let c2 = newColor 0.7 0.1 0.25
      let expected = newColor 1.6 0.7 1.0
      c1 `addedTo` c2 `shouldBe` expected

    it "can subtract colors" $ do
      let c1 = newColor 0.9 0.6 0.75
      let c2 = newColor 0.7 0.1 0.25
      let expected = newColor 0.2 0.5 0.5
      c2 `subtractedFrom` c1 `shouldBe` expected

    it "can multiply a color by a scalar" $ do
      let c = newColor 0.2 0.3 0.4
      let expected = newColor 0.4 0.6 0.8
      c `multipliedByS` 2 `shouldBe` expected

    it "can multiply two colors" $ do
      let c1 = newColor 1 0.2 0.4
      let c2 = newColor 0.9 1 0.1
      let expected = newColor 0.9 0.2 0.04
      c1 `multipliedBy` c2 `shouldBe` expected

